 
getGraphDiameterWeightedGeo <- function(shp, varNameID="neighbourhood",useWeighted=F){
  
  #Hard check
  stopifnot(varNameID %in% colnames(shp))
  varNameIDx <- paste0(varNameID,".x")
  varNameIDy <- paste0(varNameID,".y")
  
  #Create the graph from the edge list
  graph <- getGraph(shp,varNameID,useWeighted)
  
  #Get the edge list with dist
  dfAdjList <- getMatListEdgesGeo(shp, varNameID) #use the directed graph to avoid missing some edges (we only get 0.5 of edges of the direct with undirected)
  
  #Now assign the weight
  #Make sure we are not recycling some vales (e.g. if dfAdjList is obtained through a directed graph => will have twice as may edges)
 
  if(useWeighted){
    stopifnot(length( igraph::E(graph)$weight) == length( dfAdjList$dist))
    igraph::E(graph)$weight <- dfAdjList$dist %>% as.numeric()
  } else { 
    igraph::E(graph)$weight <- rep(1,length(igraph::E(graph)))
  }
 
  
  #use the directed graph to avoid missing some edges (we only get 0.5 of edges of the direct with undirected)
  dfAdjList <- getMatListEdgesGeo(shp, varNameID,useDirected = T)
  
  
  pathDiamter <- igraph::get_diameter(graph)
  
  if( !all(is.finite(pathDiamter))) stop("The graph is disconnected => diameter is Inf")
  
  #Use the nodes in the path in a semi join
  dfEdgesInDiam <- data.frame( V1 = igraph::V(graph)[pathDiamter]$name)  
  colnames(dfEdgesInDiam)[  colnames(dfEdgesInDiam) == "V1" ] <-  varNameIDx
  dfEdgesInDiam[[varNameIDx]] %<>% as.character
  dfEdgesInDiam[[varNameIDy]] <- c(dfEdgesInDiam[[varNameIDx]][2:nrow(dfEdgesInDiam)] , NA)
    
  dfAdjList %<>% semi_join(dfEdgesInDiam , by=purrr::set_names(c(varNameIDx=varNameIDx, varNameIDy = varNameIDy)))
  
  shpDiameterNeigh <- shp %>% filter_at(vars(varNameID) , all_vars(. %in% igraph::V(graph)[pathDiamter]$name ))
  shpCentroidsDiam <- shpDiameterNeigh %>% st_centroid()
  
  
  shortestPathMatrix <- igraph::shortest.paths(graph,
                         v=igraph::shortest_paths(graph,
                                                  from = pathDiamter[[length(pathDiamter)]]   , 
                                                  to = pathDiamter[[1]])$vpath[[1]]) 
  
  meltedShortestPathMatrix <- shortestPathMatrix %>% 
    as.data.frame() %>% 
    dplyr::mutate( start= rownames(shortestPathMatrix)) %>%
    gather( "end", "dist", -start)
  
  startName <- igraph::V(graph)[ pathDiamter[[1]] ]$name %>% as.character()
  endName <-  igraph::V(graph)[ pathDiamter[[length(pathDiamter)]] ]$name%>% as.character()
  
  diameterDist <- meltedShortestPathMatrix %>% dplyr::filter( grepl(startName,start)) %>% dplyr::filter( grepl(endName,end ) ) %>% pull(dist)
  
  print(paste0("Diameter is " , diameterDist))
  print(paste0("Nodes are ", paste(igraph::V(graph)[pathDiamter]$name,collapse=" -> " ) ) )
  
   
  #Get the edges
  shpEdges <- getShpEdgesFromAdjList(dfAdjList,useWeighted)
  
  listResults <- list()
  listResults[['tiles']] <- shpDiameterNeigh
  listResults[['nodes']] <- shpCentroidsDiam
  listResults[['edges']] <- shpEdges
  
  return(listResults)
}