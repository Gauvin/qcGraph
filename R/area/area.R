
getTotalAreaBothCities <- function(){
  
  shp <- miceadds::load.Rdata( filename = file.path("Data","Rdata","shpQcMtlWithMinorities.Rdata"), 
                               objname =  "shpQcMtlWithMinorities")
  
  areaQc <- getAreaPerPolygon(shp[1:35,]) %>% pull(area) %>% sum
  areaMtl <- getAreaPerPolygon(shp[36:nrow(shp),]) %>% pull(area) %>% sum
  
  writeLines(paste0("The total area is \n", 
                    areaMtl*10**-6, " km^2 for Montreal and \n",
                    areaQc*10**-6, " km^2 for Quebec"))
}

getAreaPerPolygon <- function(shp){
  
  shp$area <- st_area(shp) %>% as.double() #don't return units, this complicates things
  return(shp)
}


getDensityBothDANeigh <- function(dataset){
  
  getDADensityBothCity(dataset)
  getNeighDensityBothCity()
  
}


getDADensityBothCity <- function(dataset){
  
  shpMtl <- getQcPrDA(dataset)  %>% filter( CSD_UID == "2466023")
  shpQc <- getQcPrDA(dataset) %>% filter( CSD_UID == "2423027")
  
  shpMtlWithArea <- getAreaPerPolygon(shpMtl)
  shpQcWithArea <- getAreaPerPolygon(shpQc)
 
  densityMtl <- getPolyDensityPerCity(shpMtlWithArea,"GeoUID")
  densityQc <- getPolyDensityPerCity(shpQcWithArea,"GeoUID")
  
  writeLines(paste0("With DAs, the average area per polygon is \n", 
             densityMtl*10**-6, " km^2 for Montreal and \n",
             densityQc*10**-6, " km^2 for Quebec"))
}

getNeighDensityBothCity <- function(){
  
  shp <- miceadds::load.Rdata( filename = file.path("Data","Rdata","shpQcMtlWithMinorities.Rdata"), 
                               objname =  "shpQcMtlWithMinorities")
  
  
  densityMtl <- getPolyDensityPerCity(shp[36:nrow(shp),],"neighbourhood")
  densityQc <- getPolyDensityPerCity(shp[1:35,],"neighbourhood")
  
  writeLines(paste0("With neighbourhoods, the average area per polygon is \n", 
                    densityMtl*10**-6, " km^2 for Montreal and \n",
                    densityQc*10**-6, " km^2 for Quebec"))
}


getPolyDensityPerCity <- function(shp,varIDName){
  

  #For DAs, therehere already exists a `Shape Area` column, but it is preferable to always use the same reference for both DAs and neighbourhoods 
  shpWithArea <- getAreaPerPolygon(shp)
  totalArea <- sum(shpWithArea$area)
  
  #Get the number of plygons (taking the number of rows should yield the same results)
  totalNumberOfPoly <- shpWithArea[[varIDName]] %>% n_distinct()
    
  return(totalArea/totalNumberOfPoly) #return the average polygon area
}