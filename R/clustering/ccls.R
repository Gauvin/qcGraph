



conclustClusterQc <- function(dataset, nClusters=2, tabuIter =100){
  
  miceadds::load.Rdata(filename = here::here("Data","Rdata", "shpQcMtlWithMinorities.Rdata"), 
                       objname = "shpQcMtlWithMinorities")
  
  
  shpQcMtlWithMinorities$city <- c(rep("Qc",35),rep("Mtl",32))
  
  shpDAQc <- shpQcMtlWithMinorities %>% filter( city == "Qc")
  
  #Get the adjacency matrix + its "complement I-A"
  adjMatrix <- st_touches(shpDAQc,sparse = F)
  adjMatrixNoTouch <- (!adjMatrix) %>% as_tibble() %>% setNames(1:ncol(adjMatrix)) #use integers not V, V2, etc..
  listCols <- colnames(adjMatrixNoTouch)
  adjMatrixNoTouch %<>% mutate( startPoint = listCols )
  
  
  #Build the cannot link constraints
  dfCannotLink <- dfMelted <- adjMatrixNoTouch %>% 
    reshape2::melt( id="startPoint" ) %>% 
    filter(value==TRUE) %>% 
    dplyr::select(-value ) %>%  #remove the True column (we just want indices)
    dplyr::mutate_all(funs(as.numeric(.)))
  
  #This is a rather dense graph
  edges <- dfCannotLink %>% filter(startPoint != variable) %>% nrow #watch out treat it as directed
  max <- nrow(shpDAQc) * (nrow(shpDAQc) -1)
  
  print(paste0("Density of cannot link constraints: ", edges/max) )
  
  minorityLabelsResults <- getMinorityLabels(dataset)
  listMinorities <- minorityLabelsResults$minoritiesLabel
  listMinorities <- listMinorities[ listMinorities !="Not a visible minority"]
  
  dfMinorities <- shpDAQc %>% 
    st_set_geometry(NULL) %>% 
    dplyr::select( one_of(listMinorities ))
  
  
  
  clustResults <- conclust::ccls(dfMinorities,
                                 k=nClusters ,
                                 cantLink=as.matrix(dfCannotLink),
                                 mustLink = matrix(data=NA,nrow=1,ncol=2),
                                 tabuIter = tabuIter)
  
  dfCluster <- data.frame( neighbourhood= shpDAQc$neighbourhood,
                           clusterID= clustResults)
  
  shpDAQc %<>% left_join(dfCluster, by="neighbourhood"  )
  
  return(shpDAQc)
}




conclustClusterDA <- function(dataset, nClusters=2, tabuIter =20){
 
  
  #Read the minority data at the DA level
  shpDAQc <- getMinoritiesCensusCity(dataset,explicitNames = T) %>% filter( CSD_UID == "2423027")
  
  
  #Get the adjacency matrix + its "complement I-A"
  adjMatrix <- st_touches(shpDAQc,sparse = F)
  adjMatrixNoTouch <- (!adjMatrix) %>% as_tibble() %>% setNames(1:ncol(adjMatrix)) #use integers not V, V2, etc..
  listCols <- colnames(adjMatrixNoTouch)
  adjMatrixNoTouch %<>% mutate( startPoint = listCols )
  
  
  #Build the cannot link constraints
  dfCannotLink <- dfMelted <- adjMatrixNoTouch %>% 
    reshape2::melt( id="startPoint" ) %>% 
    filter(value==TRUE) %>% 
    dplyr::select(-value ) %>%  #remove the True column (we just want indices)
    dplyr::mutate_all(funs(as.numeric(.)))
  
  #This is a rather dense graph
  edges <- dfCannotLink %>% filter(startPoint != variable) %>% nrow #watch out treat it as directed
  max <- nrow(shpDAQc) * (nrow(shpDAQc) -1)
  
  print(paste0("Density of cannot link constraints: ", edges/max) )
  
  minorityLabelsResults <- getMinorityLabels(dataset)
  listMinorities <- minorityLabelsResults$minoritiesLabel
  listMinorities <- listMinorities[ listMinorities !="Not a visible minority"]
  
  dfMinorities <- shpDAQc %>% 
    st_set_geometry(NULL) %>% 
    dplyr::select( one_of(listMinorities ))
  
  #Watch out for NAs
  dfMinorities[is.na(dfMinorities)] <- 0
  
  clustResults <- conclust::ccls(dfMinorities,
                                 k=nClusters ,
                                 cantLink=as.matrix(dfCannotLink),
                                 mustLink = matrix(data=NA,nrow=1,ncol=2),
                                 tabuIter = tabuIter  )
  
  dfCluster <- data.frame( GeoUID= shpDAQc$GeoUID,
                           clusterID= clustResults)
  
  shpDAQc %<>% left_join(dfCluster, by="GeoUID"  )
  
  #make sure there are no geometry issues
  shpDAQc %<>% SfSpHelpers::makeSfValidProjRmZ()
  
  return(shpDAQc)
}
