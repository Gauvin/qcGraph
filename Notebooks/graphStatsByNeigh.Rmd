---
title: "Untitled"
author: "Charles"
date: "April 17, 2020"
output: html_document
---




#Librairies
```{r, message=F, echo=F, include=F,error=FALSE}

 
library(miceadds)
library(glue)
library(here)

library(sf)

library(mgcv)
library(mgcViz)
library(lme4)

library(ggplot2)
library(ggrepel)
library(gghighlight)
library(cowplot)
library(patchwork)


```

#Load all - but not test folder
```{r}
 
  GeneralHelpers::loadAll()
  Census2SfSp:::setCancensusOptions( )
```

#Parameters
```{r}

  dataset <- 'CA16'
  
  
  useMtl <- F
  varNameID <- "GeoUID"
  extraStr <- "qc"
  
  useWeighted <- F
  useDirected <- F
 
  city <- "Quebec"
  

```

---
---


#Get the DAs + neighbourhoods
```{r}
 
 
  #Get the sf sociodemo for the given census region
  shpDAWithVectors <- cancensus::get_census("CA16",
                                            level = "DA",
                                            regions =  list(CSD= "2423027" ) ,
                                            labels = "short", 
                                            geo_format = "sf")
  
  #Compute the pop density
  shpDAWithVectors %<>% mutate(ha = st_area(shpDAWithVectors) * 10**-4 ) #st_area(shpDAWithVectors) is in m^2
  shpDAWithVectors %<>% mutate(haTest = units::set_units(st_area(shpDAWithVectors), ha) )
  shpDAWithVectors %<>% mutate(popPerHa= Population/ha)

  shpNeigh <- Census2SfSp::getQcNeighbourhoods("/home/charles/Projects/Census2SfSp/Data/QuebecNeighbourhoods/")

 
  #Add the neighbourhood
  shpDAWithVectors <- st_join(shpDAWithVectors, shpNeigh %>% dplyr::select(NOM), largest=T)
  
  dfDAWithVectors <- shpDAWithVectors %>% st_set_geometry(NULL)
  
```



#Get the edges
```{r}


  matEdges <- getMatListEdgesGeoWrapper( shpDAWithVectors, 
                            varNameID = "GeoUID",
                            extraStr= "qcDAGraph")
 


```


#Compute the degree of each node 
```{r}

#The graph is undirected : we should only have rowid <= colid
# matEdges %>% filter(row.id==1) %>% pull(col.id )g %>% min :42
assertthat::are_equal( matEdges %>% mutate(isLowerDiag = row.id > col.id) %>% pull(isLowerDiag) %>% sum , 0)

#Ok undirected, and order makes no difference ->each edge should contribute 1 degree to each of the 2 endpoints 
#in and out degree do not make sense here if the graph is undirected
inDegMat <- matEdges %>% count(GeoUID.x ) %>% rename( GeoUID =  GeoUID.x)
outDegMat <- matEdges %>% count(GeoUID.y ) %>% rename( GeoUID =  GeoUID.y)

degMat <- full_join(inDegMat, outDegMat, by="GeoUID") #full join here - we don't want to lose any nodes

assertthat::are_equal(nrow(degMat) <= nrow(shpDAWithVectors), T )

#Add the number of nodes and watch out fr NAs
degMat %<>% mutate(numNeigh = rowSums(cbind(n.x,n.y),na.rm = T))

```

#Now add the density + neigh to each node
```{r}

degMat %<>% left_join(dfDAWithVectors %>% dplyr::select(GeoUID, NOM, popPerHa), by="GeoUID" )

shpDAWithVectors %<>% left_join(degMat %>% dplyr::select(GeoUID, numNeigh), by="GeoUID" ) 

```

---
---

# Plot the number of nodes per DA 


## Map

```{r}

pMapDensity <- ggplot(shpDAWithVectors) + 
  geom_sf(aes(fill=as.numeric(popPerHa)) , lwd=0) + 
  guides( fill=guide_legend( title="Population per hectare")   ) + 
  theme_minimal()

pMapDensity
```

```{r}

pMapDegree <-ggplot(shpDAWithVectors) +  
  geom_sf(aes(fill=numNeigh), lwd=0)  +
  guides( fill=guide_legend( title="Number of neighbours")   )+ 
  theme_minimal()

pMapDegree

```

```{r}


pBoth <- pMapDensity / pMapDegree

ggsave(here("Figures","degreesPerDa","mapBothDegreesDensity.png"),
       pBoth)

(pBoth)
```

#Quick models

##Gam
```{r}

degMat %<>% mutate( popPerHa=as.numeric(popPerHa) )

gamFit <- mgcv::gam( numNeigh~ s(popPerHa) , family="poisson", data=degMat)
gamFit %>% summary()
 

gamViz <- getViz(gamFit)
o <- plot( sm(gamViz, 1) )

o <- o + 
  labs(caption = "gam( numNeigh~ s(popPerHa) , family=poisson",
       subtitle = "Denser regions are smaller and have less neighbours") +
  ggtitle("Effect of population density on the number of neighbours")


png( here("Figures","degreesPerDa","gamDegreesVsDensity.png") )
print(o)
dev.off ()

```

##Mixed models should have small slope
```{r}

#Numerical errors if we do not scale the predictor - resp and covar are on 2 radically different scales
scaleMinMax <- function(x){
  (x-min(degMat$popPerHa))/(max(degMat$popPerHa)-min(degMat$popPerHa))
   }

degMat %<>% mutate(popPerHaScaled = scaleMinMax(popPerHa) )
 
mixedLmFit <- lmer( numNeigh~ popPerHaScaled + (1|NOM) , family="poisson", data=degMat)
mixedLmFit %>% summary()
 
```

# Histogram

```{r}

ggplot(degMat) + 
  geom_point(aes(x=popPerHa, y=numNeigh )) + 
  facet_wrap(~NOM)

```